<?php

return [
    /*
    | Secret key email client
    | Change 'secret_key' with your secret key.
    */
    'secret'  => 'secret_key',

    /*
    | Set className driver
    */
    'driver'  =>  \EmailSDK\Drivers\Mandrill\MandrillDriver::class
];