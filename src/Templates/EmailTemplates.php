<?php

namespace EmailSDK\Templates;

class EmailTemplates
{
    public const DEFAULT = 0;
    public const INQUIRY = 1;
    public const PENDING = 2;
    public const FAKE    = 58;
    public const UNPAID  = 59;
    public const UNPAID_TWO = 60;
    public const FAKE_TWO = 61;
    public const ONLY_REG = 62;
    public const ONLY_REG_TWO = 63;
    public const UNPAID_FOUR = 64;
    public const REGISTRATION = 65;
    public const REGISTRATION_TWO = 66;
    public const EDUMONEY_REGISTRATION_EN = 200;
    public const HELLO_EDUMONEY_EN = 201;
    public const EDUMONEY_REGISTRATION_RU = 202;
    public const HELLO_EDUMONEY_RU = 203;
    public const SYSTEM_MESSAGE_EE = 301;
    public const SYSTEM_MESSAGE_HMW = 302;
    public const SYSTEM_MESSAGE_ESSAYUSA = 303;
}