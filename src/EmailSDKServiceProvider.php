<?php

namespace EmailSDK;

use EmailSDK\Drivers\DriverConfig;
use Illuminate\Support\ServiceProvider;

class EmailSDKServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
        $this->app->bind(EmailClient::class, function ($app) {
            $config = new DriverConfig(config('email_client'));
            return new EmailClient($config);
        });
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/email_client.php' => config_path('email_client.php'),
        ]);
    }
}