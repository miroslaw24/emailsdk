<?php

namespace EmailSDK\Drivers\Mandrill;

/**
 * Class Attachment
 * @package EmailSDK\Drivers\Mandrill
 */
class Attachment
{
    /**
     * @var string the MIME type of the attachment
     */
    public $type;

    /**
     * @var string the file name of the attachment
     */
    public $name;

    /**
     * @var string the content of the attachment as a base64-encoded string
     */
    public $content;
}