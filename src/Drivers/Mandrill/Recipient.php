<?php

namespace EmailSDK\Drivers\Mandrill;

/**
 * Class Recipient
 * @package EmailSDK\Drivers\Mandrill
 */
class Recipient
{
    public $email;
    public $name;
    public $type;
}