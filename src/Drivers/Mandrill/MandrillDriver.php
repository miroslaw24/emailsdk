<?php

namespace EmailSDK\Drivers\Mandrill;

use EmailSDK\Drivers\DriverConfig;
use EmailSDK\Drivers\IDriver;
use EmailSDK\IMessage;
use Mandrill;

/**
 * Class MandrillDriver
 * @package App\Libraries\EmailSDK\Drivers\Mandrill
 */
class MandrillDriver implements IDriver
{
    protected $apiKey;
    protected $messages = [];
    protected $tags = [];
    protected $templates = [];
    protected $senders = [];

    private $config;
    private $client;

    public function __construct(DriverConfig $config)
    {
        $this->config = $config;
        $this->client = new Mandrill($this->config->getSecret());
    }

    /**
     * @return MandrillMessage
     */
    public function createMessage()
    {
        return new MandrillMessage();
    }

    /**
     * @return Recipient
     */
    public function createRecipient()
    {
        return new Recipient();
    }

    /**
     * @return Template
     */
    public function createTemplate()
    {
        return new Template();
    }

    /**
     * @return Attachment
     */
    public function createAttachment()
    {
        return new Attachment();
    }

    /**
     * @param string $template
     * @param string $content
     * @param IMessage $message
     * @param bool $async
     * @param null $ipPool
     * @param string|null $sendAt
     * @return mixed
     */
    public function sendTemplate(
        Template $template,
        IMessage $message,
        bool $async = false,
        $ipPool = null,
        string $sendAt = null
    )
    {
        if (!sizeof($template->content)) {
            $template->content = [''];
        }

        return $this->client->messages->sendTemplate($template->name, $template->content, get_object_vars($message), $async, $ipPool, $sendAt);
    }

    public function send(IMessage $message, bool $async = false, $ipPool = null, string $sendAt = null)
    {
        return $this->client->messages->send(get_object_vars($message), $async, $ipPool, $sendAt);
    }
}