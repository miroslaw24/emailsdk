<?php

namespace EmailSDK\Drivers\Mandrill;

use EmailSDK\IMessage;

/**
 * Class Message
 * @package App\Libraries\EmailSDK\Drivers\Mandrill
 */
class MandrillMessage implements IMessage
{
    public $html;
    public $text;
    public $subject;
    public $from_email;
    public $from_name;
    public $subaccount;

    /**
     * @var array extra headers to add to the message
     */
    public $headers = [];

    /**
     * @var bool whether or not this message is important
     */
    public $important;

    /**
     * @var bool whether or not to turn on open tracking for this message
     */
    public $track_opens;

    /**
     * @var bool whether or not to turn on click tracking for this message
     */
    public $track_clicks;

    /**
     * @var bool whether or not to expose all recipients in "To" header for each email
     */
    public $preserve_recipients;

    /**
     * @var bool set to false to remove content logging for sensitive emails
     */
    public $view_content_link;

    public $to = [];
    public $global_merge_vars = [];
    public $tags = [];
    public $attachments = [];
    public $images = [];

    /**
     * Add new recipient
     * @param Recipient $recipient
     * @return MandrillMessage
     */
    public function addRecipient(Recipient $recipient)
    {
        $this->to[] = [
            'email' => $recipient->email,
            'name'  => $recipient->name,
            'type'  => $recipient->type
        ];

        return $this;
    }

    /**
     * @param string $name
     * @param $content
     * @return MandrillMessage
     */
    public function addMergeVar(string $name, $content)
    {
        $this->global_merge_vars[] = [
            'name'    => $name,
            'content' => $content
        ];

        return $this;
    }

    /**
     * @param $tag
     * @return MandrillMessage
     */
    public function addTag($tag)
    {
        $this->tags[] = $tag;
        return $this;
    }

    /**
     * @param Attachment $attachment
     * @return MandrillMessage
     */
    public function addAttachment(Attachment $attachment)
    {
        $this->attachments[] = $attachment;
        return $this;
    }

    /**
     * @param Attachment $images
     * @return MandrillMessage
     */
    public function addImages(Attachment $images)
    {
        $this->images[] = $images;
        return $this;
    }
}