<?php

namespace EmailSDK\Drivers\Mandrill;

/**
 * Class Template
 * @package EmailSDK\Drivers\Mandrill
 */
class Template
{
    public $name;
    public $content = [];

    public function addContent(array $content)
    {
        $this->content[] = $content;
    }
}