<?php

namespace EmailSDK\Drivers;

use EmailSDK\IMessage;

interface IDriver
{
    public function createMessage();

    /**
     * @param IMessage $message
     * @return mixed
     */
    public function send(IMessage $message);
}