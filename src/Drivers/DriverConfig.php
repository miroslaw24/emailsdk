<?php

namespace EmailSDK\Drivers;

use EmailSDK\Exceptions\EmailDriverException;

/**
 * Class DriverConfig
 * @package EmailSDK\Drivers
 */
class DriverConfig
{
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getSecret()
    {
        return $this->getRequiredProperty('secret');
    }

    /**
     * Get class name driver
     * @return string
     * @throws EmailDriverException
     */
    public function getDriver()
    {
        return $this->getRequiredProperty('driver');
    }

    public function getRequiredProperty(string $ident)
    {
        if (!array_key_exists($ident, $this->data)) {
            throw new EmailDriverException("$ident require property!");
        }

        return $this->data[$ident];
    }
}