<?php

namespace EmailSDK;

use EmailSDK\Drivers\DriverConfig;
use EmailSDK\Drivers\IDriver;
use EmailSDK\Exceptions\EmailDriverException;

/**
 * Class EmailClient
 * @package EmailSDK
 */
class EmailClient
{
    private $driver;

    /**
     * EmailClient constructor.
     * @param string $driverName
     * @param DriverConfig $config
     * @throws EmailDriverException
     */
    public function __construct(DriverConfig $config)
    {
        $driverName = $config->getDriver();

        if (!$this->hasDriver($driverName)) {
            throw new EmailDriverException('Unsupported driver!');
        }

        $this->driver = new $driverName($config);
    }

    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param string $driverName
     * @return bool
     */
    public function hasDriver(string $driverName)
    {
        return class_exists($driverName);
    }
}